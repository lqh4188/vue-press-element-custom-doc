# Group 分组

页面分组

## 基本用法

适用于表单或视图的分隔

::: demo

```html
<template>
  <CtjGroup :name="name" :tips="'用于区分上下文模块'"></CtjGroup>
</template>

<script>
  export default {
    data() {
      return {
        name: '页面分组',
      }
    },
  }
</script>
```

:::

## Attributes 属性

| 参数 | 说明               | 类型   | 可选项 | 默认值 |
| ---- | ------------------ | ------ | ------ | ------ |
| name | 分组名称           | String |        |        |
| tips | 补充说明，默认值空 | String |        |        |
