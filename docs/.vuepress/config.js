module.exports = {
  theme: '',
  title: 'Ctj-UI',
  description: '基于Element组件搭建的后管理系统的单组件',
  base: '/',
  port: '8080',
  themeConfig: {
    nav: [
      {
        text: '首页',
        link: '/',
      },
      {
        text: '组件',
        link: '/comps/',
      },
    ],
    sidebar: {
      '/comps/': ['/comps/', '/comps/select.md', '/comps/group'],
    },
    // sidebar: {
    //   '/comps/': ['/comps/', '/comps/select.md', '/comps/inputnumber', '/comps/group', '/comps/tag-select'],
    // },
  },
  head: [],
  plugins: ['demo-container'],
  markdown: {},
}
