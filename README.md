# 项目说明

使用 vuepress 创建类自已的前端组件文档库

## 目录结构

```
.
├──docs 文档主目录
│ ├── README.md 首页
│ ├── .vuepress
│ │ ├── config.js # VuePress 核心配置文件
│ │ ├── enhanceApp.js # 组件引用配置，类似 vue 的 src/main.js
│ │ └── dist # 构建时生成的目录文件，用于部署
│ └── comps # 自定义组件文档目录 markdow 格式
│ │ ├── README.md #默认页面
│ │ └── group # 自定义分组组件
├──webui #自定义组件目录
└── package.json
```
